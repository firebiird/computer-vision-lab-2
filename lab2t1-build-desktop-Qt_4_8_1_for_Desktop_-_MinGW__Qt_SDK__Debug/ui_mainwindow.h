/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Mon 15. Oct 10:12:39 2012
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpenImg;
    QAction *actionFlipH;
    QAction *actionFlipV;
    QWidget *centralWidget;
    QWidget *gridLayoutWidget;
    QGridLayout *mainLayout;
    QLabel *imView;
    QMenuBar *menuBar;
    QMenu *menuOpen;
    QMenu *menuFlipH;
    QMenu *menuFlip_V;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1015, 625);
        actionOpenImg = new QAction(MainWindow);
        actionOpenImg->setObjectName(QString::fromUtf8("actionOpenImg"));
        actionFlipH = new QAction(MainWindow);
        actionFlipH->setObjectName(QString::fromUtf8("actionFlipH"));
        actionFlipV = new QAction(MainWindow);
        actionFlipV->setObjectName(QString::fromUtf8("actionFlipV"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayoutWidget = new QWidget(centralWidget);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 10, 991, 551));
        mainLayout = new QGridLayout(gridLayoutWidget);
        mainLayout->setSpacing(6);
        mainLayout->setContentsMargins(11, 11, 11, 11);
        mainLayout->setObjectName(QString::fromUtf8("mainLayout"));
        mainLayout->setContentsMargins(0, 0, 0, 0);
        imView = new QLabel(gridLayoutWidget);
        imView->setObjectName(QString::fromUtf8("imView"));

        mainLayout->addWidget(imView, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1015, 21));
        menuOpen = new QMenu(menuBar);
        menuOpen->setObjectName(QString::fromUtf8("menuOpen"));
        menuFlipH = new QMenu(menuBar);
        menuFlipH->setObjectName(QString::fromUtf8("menuFlipH"));
        menuFlip_V = new QMenu(menuBar);
        menuFlip_V->setObjectName(QString::fromUtf8("menuFlip_V"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuOpen->menuAction());
        menuBar->addAction(menuFlipH->menuAction());
        menuBar->addAction(menuFlip_V->menuAction());
        menuOpen->addAction(actionOpenImg);
        menuFlipH->addAction(actionFlipH);
        menuFlip_V->addAction(actionFlipV);

        retranslateUi(MainWindow);
        QObject::connect(actionOpenImg, SIGNAL(triggered()), MainWindow, SLOT(Open()));
        QObject::connect(actionFlipH, SIGNAL(triggered()), MainWindow, SLOT(flipH()));
        QObject::connect(actionFlipV, SIGNAL(triggered()), MainWindow, SLOT(flipV()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionOpenImg->setText(QApplication::translate("MainWindow", "OpenImg", 0, QApplication::UnicodeUTF8));
        actionFlipH->setText(QApplication::translate("MainWindow", "FlipH", 0, QApplication::UnicodeUTF8));
        actionFlipV->setText(QApplication::translate("MainWindow", "FlipV", 0, QApplication::UnicodeUTF8));
        imView->setText(QString());
        menuOpen->setTitle(QApplication::translate("MainWindow", "Open", 0, QApplication::UnicodeUTF8));
        menuFlipH->setTitle(QApplication::translate("MainWindow", "Flip H", 0, QApplication::UnicodeUTF8));
        menuFlip_V->setTitle(QApplication::translate("MainWindow", "Flip V", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
