#-------------------------------------------------
#
# Project created by QtCreator 2012-10-08T09:34:38
#
#-------------------------------------------------

QT       += core gui

TARGET = lab2t1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

win32 {
LIBS += -Lc:/OpenCV/bin -Lc:/OpenCV/lib -lopencv_core231 -lopencv_highgui231 \
-lopencv_imgproc231 -lopencv_features2d231 -lopencv_calib3d231
INCLUDEPATH += c:/OpenCV/include
}
