#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "QPixmap.h"
#include <QFileDialog>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{   
    ui->setupUi(this);  
    ui->centralWidget->setLayout( ui->mainLayout ); //set the main layout to be the mainLayout.
}

MainWindow::~MainWindow()
{
    delete ui;
}

int MainWindow::Open()
{

    image = cv::imread("penmon.jpg"); // reads an image file
    if( !image.data ) // check if the image has been loaded properly
    {
        std::cout << "did not load" << std::endl; // prints did not load if the image cold not be found and the aborts the program.
         return -1;
    }
    cv::cvtColor(image,image, CV_BGR2RGB );

    QImage img = QImage( (const unsigned char*)(image.data), //change the mat image to a QImage so that the label can make a pixmap out of it.
    image.cols, image.rows, image.step1(),
    QImage::Format_RGB888 );

    ui->imView->setPixmap(QPixmap::fromImage(img)); // set the label to display the image as a pixel map
    return 0;

}

void MainWindow::flipH()
{
    cv::flip(image,image,1); //reading the image then flipping it around the y axis then storing it back in the image array

    QImage img = QImage( (const unsigned char*)(image.data),
    image.cols, image.rows, image.step1(),
    QImage::Format_RGB888 );

    ui->imView->setPixmap(QPixmap::fromImage(img)); //set the flipped image to be in the pixmap

}

void MainWindow::flipV()
{
    cv::flip(image,image,0); // flip the image around the x axis, the 0 is a flipcode that reptesents how to flip the image so anything positive is around the y axis,
                             // a zero is around the x axis and a negative number is on both axes.
    QImage img = QImage( (const unsigned char*)(image.data),
    image.cols, image.rows, image.step1(),
    QImage::Format_RGB888 );

    ui->imView->setPixmap(QPixmap::fromImage(img));

}
