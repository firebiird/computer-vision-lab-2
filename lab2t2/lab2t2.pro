TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp

win32 {
LIBS += -Lc:/OpenCV/bin -Lc:/OpenCV/lib -lopencv_core231 -lopencv_highgui231 \
-lopencv_imgproc231 -lopencv_features2d231 -lopencv_calib3d231
INCLUDEPATH += c:/OpenCV/include
}
