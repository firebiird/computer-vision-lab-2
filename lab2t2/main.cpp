#include <iostream>
#include "opencv/cv.h"
#include "opencv/highgui.h"

using namespace cv;



int main()
{
    Mat image;

    image.create(500,500,CV_8UC3);


    Vec3b ul( 255, 0, 0 );
    Vec3b ur( 0, 255, 0 );
    Vec3b bl( 0, 0, 255 );
    Vec3b br( 255, 0, 255 );

    int endx = image.rows-1;
    int endy = image.cols-1;



    image.at<Vec3b>(0,0) = ul;
    image.at<Vec3b>(0,endy) = ur;
    image.at<Vec3b>(endx,0) = bl;
    image.at<Vec3b>(endx,endy) = br;


    for(int x=0;x<image.rows;x++)
    {
        for(int y=0;y<image.cols;y++)
        {
            double alpha = x/(double)image.cols;
            double alphaY = y/(double)image.rows;

            for(int z = 0; z!=3;z++){
                image.at<Vec3b>(0,x)[z] = (alpha * image.at<Vec3b>(0,endy)[z])+ (1-alpha) * image.at<Vec3b>(0,0)[z];
            }
            for(int z = 0; z!=3;z++){
                image.at<Vec3b>(endy,x)[z] = (alpha * image.at<Vec3b>(endy,endx)[z])+ (1-alpha) * image.at<Vec3b>(endy,0)[z];
            }
            for(int z = 0; z!=3;z++)
           {
                image.at<Vec3b>(y,x)[z] = (alphaY * image.at<Vec3b>(endy,x)[z])+ (1-alphaY) * image.at<Vec3b>(0,x)[z];
           }


        }
    }


    namedWindow("window 1"); // creates a new window with a specified name
    imshow("window 1",image); // shows the first image in a window called window 1
     waitKey(0); //keeps the winodw on screen untill a key is pushed.

}






